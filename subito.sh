#!/bin/bash

if  [ -z ${1} ] ; then
   echo "Pass how many times models from assets should be repeated"
   exit
fi

if  [ -z ${2} ] ; then
   echo "Second argument should be how many times repeat simulation to take average results"
   exit
fi

if [ -z ${3} ] ; then
   echo "Third argument should be te model to repeat. Peek one of the following:"
   for file in `ls ./assets/*.pl` ; do
      cropped=`echo $file | sed 's/\.\/assets\///'`
      R=`less ${file} | grep xrule | wc -l`
      A=`less ${file} | grep xattr | wc -l`
      S=`less ${file} | grep xschm | wc -l`
      echo "${cropped} (Rules: ${R}, Attributes: ${A}, Schemas: ${S}"
  done
  exit
fi

#generate models
echo 'Generating models....'
rm -r -f ./models/*
rm -r -f ./assets/multimodels/*
java -Xmx3G -jar model-gen.jar ${1} ${3} 2> /home/sbk/model-gen-log.txt
cd ./assets
echo "Generateing states..."
echo "for Jess"
./ct-states.sh jess
echo "for Java rule engine"
./ct-states.sh jre
echo "for Drools"
./ct-states.sh drl
echo "for Context-toolkit"
./ct-states.sh ct
echo "for Easy Rules"
./ct-states.sh er
mkdir -p ../models
cp -R ./multimodels/* ../models/
cd ..
echo 'Models generated....'
echo 'Cmpiling runners....'
cd context-toolkit
echo 'for Context-Toolkit'
./compile-runners.sh 2> /dev/null
cd ../java-rule-engine
echo 'for Java Rule Engine'
./compile-runners.sh 2> /dev/null
cd ../easy-rules
echo 'for Easy Rules'
./compile-runners.sh 2> /dev/null
cd ..
echo 'Runners compiled....'
echo 'Generating benchmark data....'

./generate_benchmark_data.sh ${2} ${3}

echo 'Benchmark data generated. Run plot_stats from octave in folder plots'
