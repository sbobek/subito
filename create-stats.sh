#!/bin/bash

if  [ -z ${1} ] ; then
   echo "Pass log file as a parameter"
fi

echo 'RULES,ATTS,SCHMS,HEARTDROID_TIME,HEARTDROID_MEM ,DROOLS_TIME, DROOLS_MEM,JESS_TIME,JESS_MEM,TUHEART_TIME,TUHEART_MEM,CT_TIME,CT_MEM,JRE_TIME,JRE_MEM,ER_TIME,ER_MEM'

RNO=($(less ${1} | grep "rules;" | cut -f2 -d ' '))
ANO=($(less ${1} | grep "attributes;" | cut -f4 -d ' '))
SNO=($(less ${1} | grep "schemas;" | cut -f6 -d ' '))
HEART_TIME=($(less ${1} | grep " HRTD:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
HEART_MEM=($(less ${1} | grep " HRTD:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

DRL_TIME=($(less ${1} | grep " DROOLS:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
DRL_MEM=($(less ${1} | grep " DROOLS:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

JESS_TIME=($(less ${1} | grep " JESS:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
JESS_MEM=($(less ${1} | grep " JESS:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

TUHRT_TIME=($(less ${1} | grep " TUHRTD:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
TUHRT_MEM=($(less ${1} | grep " TUHRTD:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

CT_TIME=($(less ${1} | grep " CT:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
CT_MEM=($(less ${1} | grep " CT:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

JRE_TIME=($(less ${1} | grep " JRE:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))
JRE_MEM=($(less ${1} | grep " JRE:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))

ER_TIME=($(less ${1} | grep " ER:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f3 -d ' '))

ER_MEM=($(less ${1} | grep " ER:  [0-9]" | sed 's/^[0-9]*[ ]*//g' | sed 's/;/ /g' | cut -f5 -d ' '))



counter=0
for rno in ${RNO[@]} ; do
  echo $rno','${ANO[$counter]}','${SNO[$counter]}','${HEART_TIME[$counter]}','${HEART_MEM[$counter]}','${DRL_TIME[$counter]}','${DRL_MEM[$counter]}','${JESS_TIME[$counter]}','${JESS_MEM[$counter]}','${TUHRT_TIME[$counter]}','${TUHRT_MEM[$counter]}','${CT_TIME[$counter]}','${CT_MEM[$counter]}','${JRE_TIME[$counter]}','${JRE_MEM[$counter]}','${ER_TIME[$counter]}','${ER_MEM[$counter]}
counter=$counter+1

done 

