#!/bin/bash

for file in `ls ${1} | sed 's/\(.pl\|\-jre\|.clp\|.drl\|-EasyRules\|.clips\|-DTRules\)$//g' | uniq` ; do
./execute-model.sh ${1}$file
done
