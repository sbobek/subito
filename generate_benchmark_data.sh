#!/bin/bash

LIST=""
NAME=`echo ${2} | sed 's/\.pl//' | sed 's/-/_/g'`
mkdir -p logs
#rm -f ./logs/*
#rm -f ./plots/*.csv
for ((i=0; i<${1}; i++)) ; do
  echo "Generating stats for run ${i} for model ${2}"
  ./execute-all-models.sh models/ > ./logs/log-${NAME}-$i
  ./create-stats.sh ./logs/log-${NAME}-$i > ./plots/stats-${NAME}-${i}'.csv'
  LIST=${LIST}"\'stats-${NAME}-${i}.csv\';"
done

LIST=${LIST::-1}

sed "s/datasets = \\[.*\\];$/datasets = [${LIST}];/g" ./plots/plot_stats.m > ./plots/plot_stats_${NAME}.m

