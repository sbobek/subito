#!/bin/bash

if [ -z ${1} ] ; then
  echo "Not enough arguments"
  exit
fi

cd multimodels

for model in `ls  *.pl` ; do
  file=`echo $model | sed 's/\.pl//g'`

  STATES=`less ${model} | grep xstat`
  R=`echo $STATES | tr '.' '\n' | sed 's/.*: //g' | sed 's/\[//g' | sed 's/\]//g'`
  
  if [ "${1}" = "ct" ] ; then
    cp ../Runner.java-template $file'/Runner.java'
  fi

  for state in `echo $R | sed 's/[ ]/\n/g'` ; do
    att=`echo $state | cut -d ',' -f1`
    val=`echo $state | cut -d ',' -f2`
    re='^-?[0-9]+([.][0-9]+)?$'
    int='^-?[0-9]+$'
    if  [[ $val =~ $int ]] ; then
       val=${val}'.0'
    elif  ! [[ $val =~ $re ]] &&  ! [[ "${1}" = "jess" ]]   ; then
       val='"'${val}'"'
    fi
    if [ "${1}" = "ct" ] ; then
      sed "/\/\*DATA\*\//a allWidget.updateData(\"$att\", $val);" "./${file}/Runner.java" > "./${file}/Runner.java-stateful"
      cat "./${file}/Runner.java-stateful" > "./${file}/Runner.java"
      rm -f "./${file}/Runner.java-stateful"
    elif [ "${1}" = "jre" ] ; then
      awk -v att='__'$att'__' -v var=$val '{ gsub(att, var) ;  print}' "./${file}-jre/state-snippet" > "./${file}-jre/state-temp"
      cat "./${file}-jre/state-temp" > "./${file}-jre/state-snippet"
      rm "./${file}-jre/state-temp"
    elif [ "${1}" = "jess" ] ; then
      sed "/;DATA/a (assert($att ( value $val)))" "./${file}.clp" > "./${file}.clp-stateful"
      cat "./${file}.clp-stateful" > "./${file}.clp"
      rm -f "./${file}.clp-stateful"
    elif [ "${1}" = "drl" ] ; then
      sed "/\/\*DATA\*\//a insert(new $att($val));" "./${file}.drl" > "./${file}.drl-stateful"
      cat "./${file}.drl-stateful" > "./${file}.drl"
      rm -f "./${file}.drl-stateful"
    elif [ "${1}" = "er" ] ; then
      awk -v att='__'$att'__' -v var=$val '{ gsub(att, var) ;  print}' "./${file}-EasyRules/state-snippet" > "./${file}-EasyRules/state-temp"
      cat "./${file}-EasyRules/state-temp" > "./${file}-EasyRules/state-snippet"
      rm "./${file}-EasyRules/state-temp"
    fi
  done
  if [ "${1}" = "jre" ] ; then
      cp "../RunnerJRE.java-template" "${file}-jre/RunnerJRE.java"
      # remove all missing states
      sed -i '/__.*__/d' "./${file}-jre/state-snippet"
      # copy all state snippets to Java code
      states=`cat "./${file}-jre/state-snippet"`
      states=`echo $states`
      sed "\/\*DATA\*\//a ${states}" "./${file}-jre/RunnerJRE.java" >"./${file}-jre/RunnerJRE.java-stateful"
      cat "./${file}-jre/RunnerJRE.java-stateful" > "./${file}-jre/RunnerJRE.java"
      rm "./${file}-jre/RunnerJRE.java-stateful"
   elif [ "${1}" = "er" ] ; then
      sed -i '/__.*__/d' "./${file}-EasyRules/state-snippet"
      # copy all state snippets to Java code
      states=`cat "./${file}-EasyRules/state-snippet"`
      states=`echo $states`
      sed "\/\*DATA\*\//a ${states}" "./${file}-EasyRules/starts/Start.java" >"./${file}-EasyRules/starts/Start.java-stateful"
      cat "./${file}-EasyRules/starts/Start.java-stateful" > "./${file}-EasyRules/starts/Start.java"
      rm "./${file}-EasyRules/starts/Start.java-stateful"
  fi

done
