#!/bin/bash

for file in `ls -d ../models/*/` ; do
  `javac -cp $file:./lib/* $file'Runner.java'`
  echo '$file*~' | xargs | rm
 
done
