%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONFIGURATION SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Operators
%
% structural operators
:-op(830,fx,xrule).         % used to define rule
:-op(830,fx,xtype).         % used to define type
:-op(830,fx,xattr).         % used to define attribute
:-op(830,fx,xschm).         % used to define scheme
:-op(830,fx,xstat).         % used to define state
:-op(830,fx,xtpgr).         % used to define group of attributes
:-op(830,fx,xatgr).         % used to define group of attributes
:-op(830,fx,xcall).         % used to define callbacks
:-op(830,fx,xactn).	        % used to define actions
:-op(830,fx,xtraj).         % used to define trajectory
%

:-op(800,xfx,'>>>').        % used to define actions
:-op(800,xfx,'==>').		% used to separate LHS and RHS of rule
:-op(500,xfx,'**>').		% used to indicate the actions in the decision part
:-op(499,xfx,':').          % For HeaRTDroid
:-op(499,xfx,'/').          % For HeaRTDroid


% alvs operators
:-op(300,xfy,eq).
:-op(300,xfy,neq).
:-op(300,xfy,in).
:-op(300,xfy,notin).
:-op(300,xfy,subseteq).
:-op(300,xfy,supseteq).
:-op(300,xfy,sim).
:-op(300,xfy,notsim).

:-op(816,xfx,to).  % used to specify ranges for general ordered atts, as <mon,fri> [mon to fri]
:-op(300,xfy,set). % setting val in decision

:-op(301,xfy,gt).
:-op(301,xfy,lt).
:-op(301,xfy,gte).
:-op(301,xfy,lte).


flag_debug(0).  %Debug level for HeaRT 0 - silent, 3 - loud
trajectory_mode(full).  %can be full, table simple
precision(10).  %Precision for floating point numbers

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MAIN SHELL SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gox(StateIn,T,foi) :-
	traj_genid(_),
	state_print(StateIn),
	state_copy(StateIn,current),
	trajectory_projection(start,_), 
	runtables(T),
	trajectory_projection(end,simple), 
	state_print(current),!.

gox(StateIn,T,ddi) :-
	traj_genid(_),
	state_print(StateIn),
	state_copy(StateIn,current),
	trajectory_projection(start,_), 
	runrules_DDI(T),
	trajectory_projection(end,simple), 
	state_print(current),!.

gox(StateIn,T,tdi) :-
	traj_genid(_),
	state_print(StateIn),
	state_copy(StateIn,current),
	trajectory_projection(start,_), 
	runrules_TDI(T),
	trajectory_projection(end,simple), 
	state_print(current),!.
	
gox(StateIn,T,gdi) :-
	traj_genid(_),
	state_print(StateIn),
	state_copy(StateIn,current),
	trajectory_projection(start,_), 
	runrules_GDI(T),
	trajectory_projection(end,simple), 
	state_print(current),!.

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALSV(FD) SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	


alsv_valid(Att eq any, State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]),
	heart_debug(2,['Valid:',Att,eq,any]).

alsv_valid(Att in [any], State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]),
	heart_debug(2,['Valid:',Att,in,[any]]).
	
alsv_valid(Att eq any, State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	heart_debug(2,['Valid:',Att,eq,any]).

alsv_valid(Att eq [any], State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	heart_debug(2,['Valid:',Att,eq,[any]]).

alsv_valid(Att eq null, State) :-
	alsv_values_get(State,Att,StateValue),
	!,
	\+ alsv_values_get(State,Att,StateValue), 
	heart_debug(2,['Valid:',Att,eq,null]).
	
alsv_valid(Att eq null, State) :-
	alsv_values_get(State,Att,StateValue),
	!,
	StateValue = null, 					   
	heart_debug(2,['Valid:',Att,eq,null]).

alsv_valid(Att in [L to U], State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[L]), alsv_values_check(Att,[U]),
	normalize(Att,[StateValue],[NormStateValue]),
	normalize(Att,[L],[NormL]),
	normalize(Att,[U],[NormU]),
	NormStateValue =< NormU,
	NormStateValue >= NormL,		
	heart_debug(2,['Valid:',Att,in,[L to U]]).
	
alsv_valid(Att notin [L to U], State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[L]), alsv_values_check(Att,[U]),
	normalize(Att,[StateValue],[NormStateValue]),
	normalize(Att,[L],[NormL]),
	normalize(Att,[U],[NormU]),
	NormStateValue >= NormU,
	NormStateValue =< NormL,	
	heart_debug(2,['Valid:',Att,notin,[L to U]]).
	
alsv_valid(Att in SetValue, State) :-		
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), 	
	alsv_values_check(Att,SetValue),
	normalize(Att,[StateValue],[NormStateValue]),
	normalize(Att,SetValue,NormSetValue),
	(member(NormStateValue,NormSetValue);math_member(NormStateValue,NormSetValue)),		
	heart_debug(2,['Valid:',Att,in,SetValue]).
	
alsv_valid(Att notin SetValue, State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,SetValue),
	normalize(Att,[StateValue],[NormStateValue]),
	normalize(Att,SetValue,NormSetValue),
	\+ member(NormStateValue,NormSetValue),
	\+ math_member(NormStateValue,NormSetValue),
	heart_debug(2,['Valid:',Att,notin,SetValue]).
	
alsv_valid(Att eq Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_domain(Att,_,numeric),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue =:= NormStateValue,
	heart_debug(2,['Valid:',Att,eq,Value]).
	
alsv_valid(Att eq Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue == NormStateValue,
	heart_debug(2,['Valid:',Att,eq,Value]).
	
alsv_valid(Att neq Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue \= NormStateValue,
	heart_debug(2,['Valid:',Att,neq,Value]).

alsv_valid(Att gt Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_domain(Att,_,numeric),	
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue < NormStateValue,
	heart_debug(2,['Valid:',Att,gt,Value]).
	
alsv_valid(Att gte Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_domain(Att,_,numeric),	
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue =< NormStateValue,
	heart_debug(2,['Valid:',Att,gte,Value]).
	
alsv_valid(Att lt Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_domain(Att,_,numeric),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue > NormStateValue,
	heart_debug(2,['Valid:',Att,lt,Value]).
	
alsv_valid(Att lte Value, State) :-
	alsv_attr_class(Att,simple),
	alsv_domain(Att,_,numeric),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,[StateValue]), alsv_values_check(Att,[Value]),
	normalize(Att,[Value],[NormValue]),
	normalize(Att,[StateValue],[NormStateValue]),
	NormValue >= NormStateValue,
	heart_debug(2,['Valid:',Att,lte,Value]).
	
%%% Table2: cases of \eq, \neq, \subseteq, \supseteq,\sim, \notsim  versus set StateValue in State
alsv_valid(Att eq Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	eqset(NormSet,NormStateValue),
	heart_debug(2,['Valid:',Att,eq,Set]).
	
alsv_valid(Att neq Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	neqset(NormSet,NormStateValue),
	heart_debug(2,['Valid:',Att,eq,Set]).
	
alsv_valid(Att subseteq Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	alsv_subset(NormStateValue,NormSet),
	heart_debug(2,['Valid:',Att,subset,Set]).
	
alsv_valid(Att supseteq Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	alsv_subset(NormSet,NormStateValue),
	heart_debug(2,['Valid:',Att,supset,Set]).
	
alsv_valid(Att sim Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	simset(NormStateValue,NormSet),		
	heart_debug(2,['Valid:',Att,sim,Set]).
	
alsv_valid(Att notsim Set,State) :-
	alsv_attr_class(Att,general),
	alsv_values_get(State,Att,StateValue),
	!,
	alsv_values_check(Att,StateValue),
	alsv_values_check(Att,Set),
	normalize(Att,StateValue,NormStateValue),
	normalize(Att,Set,NormSet),
	\+simset(NormStateValue,NormSet),		
	heart_debug(2,['Valid:',Att,notsim,Set]).

alsv_valid(F,_) :-
	heart_debug(2,['Check for:',F,'NYI!']),
	fail.
	
alsv_values_get(State,Att,StateValue) :-
	xstat State: [Att,StateValue],!.
	
alsv_values_get(_,Att,StateValue) :-
	xattr(A),
	member(name:Att,A),
	member(comm:C,A),
	(C=in;C=inter),
	fire_callback(Att),			
	xstat current: [Att,StateValue].
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% alsvd checks
alsv_values_check(_,[]).

	
%domain is set, but value X to Y
alsv_values_check(Att,[LS to US|Rest]) :-
	alsv_domain(Att,Domain,numeric), 
	heart_debug(2,['Check:',Att,numericreg,LS to US]),		
	math_member([LS to US],Domain),
	alsv_values_check(Att,Rest),
	!.
	
alsv_values_check(Att,[StateValue|Rest]) :-
	nonvar(StateValue),	
	alsv_domain(Att,Domain,numeric),
	heart_debug(2,['Check:',Att,numeric,StateValue]),	
	math_member(StateValue,Domain), % check if value in the domain with math_memeber
	alsv_values_check(Att,Rest),
	!.
	
%domain is set, but value X to Y
alsv_values_check(Att,[LS to US|Rest]) :-
	alsv_order(Att,yes),
	alsv_domain(Att,Domain,symbolic), 
	heart_debug(2,['Check:',Att,symbolic,LS to US]),		
	normalize(Att,Domain,NDomain),	
	normalize(Att,LS,NLS),
	normalize(Att,US,NUS),
	sort(NDomain, [First|SNDomain]),
	last(SNDomain,Last),
	NLS >= First,
	NUS =< Last,
	alsv_values_check(Att,Rest),
	!.
		
alsv_values_check(Att,[StateValue|Rest]) :-
	nonvar(StateValue),		
	alsv_domain(Att,Domain,symbolic),
	heart_debug(2,['Check:',Att,symbolic,StateValue]),
	alsv_order(Att,yes),
	number(StateValue),
	math_member(StateValue,Domain), %chceck if weight in the domain with math_memeber
	alsv_values_check(Att,Rest),
	!.
	
alsv_values_check(Att,[StateValue|Rest]) :-
	nonvar(StateValue),		
	alsv_domain(Att,Domain,symbolic),
	heart_debug(2,['Check:',Att,symbolic,StateValue]),
	alsv_order(Att,yes),
	member(StateValue/_,Domain),
	alsv_values_check(Att,Rest),
	!.
	
alsv_values_check(Att,[StateValue|Rest]) :-
	nonvar(StateValue),		
	alsv_domain(Att,Domain,symbolic),
	heart_debug(2,['Check:',Att,symbolic,StateValue]),
	alsv_order(Att,yes),
	number(StateValue),
	truncate(StateValue) =:= StateValue,
	TruncatedValue is truncate(StateValue),
	nth1(TruncatedValue,Domain,_),
	alsv_values_check(Att,Rest),
	!.
	
alsv_values_check(Att,[StateValue|Rest]) :-		
	alsv_domain(Att,Domain,symbolic),
	heart_debug(2,['Check:',Att,symbolic,StateValue]),
	member(StateValue,Domain),
	alsv_values_check(Att,Rest),
	!.

% FIXME alsv_values_check for general atts missing! ex. [1,2,3,4,5] domain [1,2,3,4,5,6,7,8]. subset

alsv_values_check(Att,[V|_]) :-
	write('  ERROR: values of \"'), write(Att),
	write('\": '), write(V), write(' NOT in the domain!'),nl,!.

%retrieves attribute domain, and base: symbolic, numeric
alsv_domain(Att,Domain,Base) :-
	xattr(A),
	member(name:Att,A),
	member(type:T,A),
	xtype(Ts),
	member(name:T,Ts),
	member(domain:Domain,Ts),
	member(base:Base,Ts).
	
%Do not scale nor normalize domains that are for instance january/1, february/2 
alsv_normalized_domain(Att,Domain,Base) :-
   alsv_domain(Att,RawDomain,Base),
   normalize_all(Att,RawDomain,Domain).

% retrieves attribute class: simple, general
alsv_attr_class(Att,Class) :-
	xattr(A),
	member(name:Att,A),
	member(class:Class,A).

%retrieves attribute order type FIXME, does not work if wnat just check
alsv_order(Att,yes) :-
	xattr(A),
	member(name:Att,A),
	member(type:T	,A),
	xtype(Ts),	
	member(name:T,Ts),
	member(base:numeric, Ts),!.

alsv_order(Att,Order) :-
	xattr(A),
	member(name:Att,A),
	member(type:T,A),
	xtype(Ts),
	member(name:T,Ts),
	member(ordered:Order,Ts),!.

alsv_order(_,no).
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

alsv_transition(Att set Expr) :-
%	atom(Val),
	\+ Expr = [_|_],
	alsv_domain(Att,_,numeric),
	alsv_attr_class(Att,simple),
	alsv_eval_simple(Expr, Val),
	heart_debug(2,['Adding new fact: ',Att,' set to ',Val]),
	alsv_new_val(Att,Val).

alsv_transition(Att set Val) :-
	\+ Val = [_|_],
	alsv_domain(Att,_,symbolic),
	alsv_attr_class(Att,simple),
	heart_debug(2,['Adding new fact: ',Att,' set to ',Val]),
	alsv_new_val(Att,Val).

alsv_transition(Att set Expr) :-
	Expr = [_|_],
	alsv_eval_general(Expr,Val),
	alsv_attr_class(Att,general),
	alsv_new_val(Att,Val).

% fixme: no direct assert/retract, but set/get_att_val
alsv_new_val(A,V):-
	retractall(xstat current:[A,_]),
	assert(xstat current:[A,V]),
	heart_debug(2,['Trans:',A,to,V]).

alsv_eval_simple(Expr, Evaluated) :-
	parse(Expr, Parsed),
	Resolved =.. Parsed,
	Evaluated is Resolved.

alsv_eval_general(Expr, Expr) :-
	Expr = [H|_],!.                 %check if Expr is a list, as is_list is not working in tuProlog

alsv_eval_general(Expr, Eval) :-
	Expr = union(V1,V2),
	alsv_eval_general(V1,R1),
	alsv_eval_general(V2,R2),
	alsv_union(R1,R2,Eval),!.

alsv_eval_general(Expr, Eval) :-
	Expr = intersect(V1,V2),
	alsv_eval_general(V1,R1),
	alsv_eval_general(V2,R2),
	alsv_intersect(R1,R2,Eval),!.

alsv_eval_general(Expr, Eval) :-
	Expr = except(V1,V2),
	alsv_eval_general(V1,R1),
	alsv_eval_general(V2,R2),
	alsv_except(R1,R2,Eval),!.

alsv_eval_general(Expr, Eval) :-
	Expr = complement(Att,V2),
	alsv_eval_general(V2,R2),
	alsv_complement(Att,R2,Eval),!.

parse(Expr, Parsed) :-
	Expr =.. List,
	parse_helper(List,Parsed),!.	

parse_helper([],[]).

parse_helper([H|Rest],[Val|Parsed]) :-
	parse_is_attr(H),
	alsv_values_get(current,H,Val),
	parse_helper(Rest,Parsed),!.
	
parse_helper([H|Rest],[H|Parsed]) :-
	number(H),
	parse_helper(Rest, Parsed),!.	

parse_helper([H|Rest],[H|Parsed]) :-
	parse_is_operator(H),
	parse_helper(Rest,Parsed),!.

parse_helper([SubExpr|Rest], [Unsplit|Parsed]) :-
	SubExpr =.. Split,
	parse_helper(Split,Subparsed),
	Unsplit =.. Subparsed,
	parse_helper(Rest, Parsed),!.


parse_is_operator(H) :-
	H = '-';
	H = '+';
	H = '*';
	H = '/';
	H = '(';
	H = ')'.

parse_is_attr(H) :-
	xattr(P),
	member(name:H,P),!.
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% UTIL SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	
%debug_set_flag(+Val)
%Predicat that sets the debug level.
%It takes one parameter that is the integer value from 0 to 3, where 0 means no details, and 3 means all details.
debug_set_flag(Val) :-
	retractall(flag_debug(_)),
	assert(flag_debug(Val)).


%get_explicit_value(+Att,+NormValue,?ExplicitValue)
%In case an attiribute is ordered it is stored in HeaRT memory in its numerical representation
%The predicate is used to retriev its symbolic value (for instance january instead of 1.0)
get_explicit_value(Att,[NLB to NUB],[LB to UB]) :-
	get_explicit_value(Att,NLB,LB),
	get_explicit_value(Att,NUB,UB).

get_explicit_value(Att,NormValue,ExplicitValue) :-
	alsv_normalized_domain(Att,NormDomain,symbolic),
	alsv_domain(Att,Domain,_),
	nth0(Index,NormDomain,NormValue),
	nth0(Index,Domain,ExplicitValue),!.
	
%For states gives as a single values but still in brackets, eg [day,[6.0]]
get_explicit_value(Att,[NormValue],ExplicitValue) :-
	alsv_normalized_domain(Att,NormDomain,symbolic),
	alsv_domain(Att,Domain,_),
	nth0(Index,NormDomain,NormValue),
	nth0(Index,Domain,ExplicitValue),!.
	
get_explicit_value(Att,NormValue,NormValue) :-
	alsv_domain(Att,_,numeric).
	

heart_debug(N,D) :-
	flag_debug(L),
	number(L),
	N =< L,
	write('DEBUG('),
	write(N), write('): '),
	heart_debug_process(D),!.
heart_debug(_,_).

heart_debug_process([]) :- nl,!.
heart_debug_process([H|T]):-
	write(H), write(' '),!,
	heart_debug_process(T).


%%%%%%%%%%%%%%%%%%% ALSV(FD) UTILITIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%
normalize_all(_,[],[]).
normalize_all(Att,[Value|RawRest],[Output|Rest]) :-
     	normalize(Att,[Value],[Output]),
        normalize_all(Att,RawRest,Rest).

normalize(_,[],[]).
	
normalize(Att, Value, Output) :-
	\+ alsv_order(Att,yes),
	alsv_domain(Att,Domain,_),
	member(Value,Domain),
	Value = Output,!.
	
normalize(Att, Value, Output) :-
	alsv_order(Att,yes),
	number(Value),			%and change output to floats if number (can be outside the domain)
	alsv_domain(Att,Domain,_),
	math_member(Value,Domain),	
	Output is Value*1.0,!.

normalize(Att, Value, Output) :-	
	alsv_order(Att,yes),
	alsv_domain(Att,Domain,_),
	member(Value/Numbering,Domain), 
	Output is Numbering*1.0,!.

normalize(Att, MixedValue, Output) :-	
	alsv_order(Att,yes),
	alsv_domain(Att,Domain,_),
	member(MixedValue,Domain),
	MixedValue = _/Numbering, 
	Output is Numbering*1.0,!.
	
normalize(Att, Value, Output) :-
	alsv_order(Att,yes),
	alsv_domain(Att,Domain,_),
	atom(Value),			%and change output to floats if number
	nth1(Number,Domain,Value),
	Output is Number*1.0.
	
normalize(Att, Value, Output) :-
	alsv_order(Att,yes),
	alsv_domain(Att,Domain,_),
	number(Value),
	truncate(Value) =:= Value,
	TruncatedNumber is truncate(Value),
	nth1(TruncatedNumber,Domain,_),	%and change output to floats
	Output is TruncatedNumber*1.0,!.

normalize(Att,[X to Y | Rest1],Rest2) :-
	%alsv_domain(Att,_,numeric),	
	normalize(Att,X,XN),
	normalize(Att,Y,YN),
	normalize(Att,Rest1,Rest),
	merge([XN to YN],Rest,Rest2),
	!.
	

normalize(Att,[Number|Rest1],[Value|Rest2]) :-
	normalize(Att,Number,Value),
	normalize(Att,Rest1,Rest2),
	!.
	
		

math_member(Number, [Lower to Upper|_]) :-
	number(Number),
	number(Lower),
	number(Upper),
	Number =< Upper,
	Number >= Lower,!.


math_member(Number, [Lower to Upper|Rest]) :-
	number(Number),
	number(Lower),
	number(Upper),
	math_member(Number,Rest),!.

math_member(L to U, Set) :-
	Set = [_|_],                %check if Expr is a list, as is_list is not working in tuProlog
	member(L,Set),
	member(U,Set),!.

math_member(L to U,[Number|Rest]) :-
	number(Number),
	math_member(L to U,Rest),!.

math_member(L to U,[SL to SU|_]) :-
	number(L),
	number(U),
	number(SL),
	number(SU),
	L >= SL,
	U =< SU,!.

math_member(L to U,[SL to SU|Rest]) :-
	number(L),
	number(U),
	number(SL),
	number(SU),
	math_member(L to U,Rest),!.


math_member(Weight,[_/DomainWeight|[]]) :-
	number(Weight),
	number(DomainWeight),
	Weight =:= DomainWeight,!. 

math_member(Weight,[_/DomainWeight|Rest]) :-
	number(Weight),
	number(DomainWeight),
	Weight =\= DomainWeight, 
	math_member(Weight,Rest),!.
	
math_member(Weight,[_/DomainWeight|_]) :-
	number(Weight),
	number(DomainWeight),
	Weight =:= DomainWeight,!.
	
math_member(Number,[Head|[]]) :-
	number(Number),
	number(Head),
	Number =:= Head,!. 
math_member(Number,[Head|Rest]) :-
	number(Number),
	number(Head),
	Number =\= Head, 
	math_member(Number,Rest),!.
	
math_member(Number,[Head|_]) :-
	number(Number),
	number(Head),
	Number =:= Head,!.


math_member([],_).

math_member([Head|Rest],Set) :-
	math_member(Head,Set),
	math_member(Rest,Set),!.

alsv_subset(Set1, Set2) :-
	subset(Set1,Set2),!.
alsv_subset(Set1,Set2) :-
	math_member(Set1,Set2),!.

eqset(Set1,Set2) :-
	alsv_subset(Set1,Set2),
	alsv_subset(Set2,Set1),!.

neqset(Set1,Set2) :-
	\+ eqset(Set1,Set2).

simset(Set1, Set2) :-
	\+ sub(Set1, Set2, Set1).

onesub([],_,[]).	

onesub([L1 to U1|Set1], [L2 to U2],[L1 to NU1, NL2 to U1|R]) :-
	L2 > L1,
	U2 < U1,
	NU1 is L2-1,
	NL2 is U2+1,
	onesub(Set1,[L2 to U2],R),!.

onesub([L1 to U1|Set1], [L2 to U2],[NL1 to U1|R]) :-
	U2 =:= L1,
	U1 > U2,
	NL1 is L1+1,
	onesub(Set1,[L2 to U2],R),!.

onesub([L1 to U1|Set1], [L2 to U2],[L1 to NU1|R]) :-
	U1 =:= L2,
	U2 > U1,
	NU1 is U1-1,
	onesub(Set1,[L2 to U2],R),!.


onesub([L1 to U1|Set1], [L2 to U2],[L1 to NU1|R]) :-
    	L1 < L2,
	L2 < U1,
	U2 >= U1,
	NU1 is L2-1,
	onesub(Set1,[L2 to U2],R),!.

onesub([L1 to U1|Set1], [L2 to U2],[NL1 to U1|R]) :-
	U1 > U2,
	L1 < U2,
	L2 =< L1,
	NL1 is U2+1,
	onesub(Set1,[L2 to U2],R),!.

onesub([L1 to U1|Set1], [L2 to U2],[L2 to NU1|R]) :-
	L2 < L1,
    	L1 < U2,
	U2 =< U1,
    	NU1 is L1-1,
	onesub(Set1,[L2 to U2],R),!.

onesub([L1 to U1|Set1],[L2 to U2], R) :-
	L2 =< L1,
	U1 =< U2,
	onesub(Set1,[L2 to U2],R),!.


onesub([L1 to U1|Set1],[Number],[NL to U1|R]) :-
	L1 \= U1,  
	number(Number),
	Number =:= L1,
	NL is Number+1,
    NL \= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[NL|R]) :-
	L1 \= U1,  
	number(Number),
	Number =:= L1,
	NL is Number+1,
    NL =:= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[L1 to NU|R]) :-
	L1 \= U1,
	number(Number),
	Number =:= U1,
	NU is Number-1,
    NU \= L1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[NU|R]) :-
	L1 \= U1,
	number(Number),
	Number =:= U1,
	NU is Number-1,
    NU =:= L1,
	onesub(Set1, [Number],R),!.


onesub([L1 to U1|Set1],[Number],[L1 to NU, NL to U1|R]) :-
	L1 \= U1,
	number(Number),
	math_member(Number, [L1 to U1]),
	NU is Number-1,
	NL is Number+1,
    NU \= L1,
    NL \= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[NU, NL to U1|R]) :-
	L1 \= U1,
	number(Number),
	math_member(Number, [L1 to U1]),
	NU is Number-1,
	NL is Number+1,
    NU =:= L1,
    NL \= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[L1 to NU, NL|R]) :-
	L1 \= U1,
	number(Number),
	math_member(Number, [L1 to U1]),
	NU is Number-1,
	NL is Number+1,
    NU \= L1,
    NL =:= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[NU, NL|R]) :-
	L1 \= U1,
	number(Number),
	math_member(Number, [L1 to U1]),
	NU is Number-1,
	NL is Number+1,
    NU =:= L1,
    NL =:= U1,
	onesub(Set1, [Number],R),!.

onesub([L1 to U1|Set1],[Number],[L1 to U1|R]) :-
	L1 \= U1,
	number(Number),
	\+math_member(Number, [L1 to U1]),
	onesub(Set1, [Number],R),!.


onesub([Number|Set1],[L2 to U2],[Number|R]) :-
	number(Number),
	\+ math_member(Number, [L2 to U2]),
	onesub(Set1, [L2 to U2],R),!.

onesub([Number1|Set1],[Number2],[Number1|R]) :-
	number(Number1),
	number(Number2),
	Number1 =\= Number2,	
	onesub(Set1, [Number2],R),!.

onesub([Element1|Set1],[Element2],[Element1|R]) :-
	\+number(Element1),
	\+number(Element2),
	Element1 \= Element2, 
	onesub(Set1,[Element2],R),!.

onesub( [_| Set1],E,R) :-
	onesub(Set1,E,R),!.

sub(R,[],R).

sub(Set1,[H|Set2],R) :-
	onesub(Set1, [H], RR),
	sub(RR,Set2, R),!.

alsv_intersect_all([X|[]],[X|[]]).
alsv_intersect_all([H|RestSet], Result) :-
    alsv_intersect_all(RestSet, RestResult), 
    flatten(H,FH),
    flatten(RestResult,FRestResult),
    alsv_intersect(FH, FRestResult, Result).
	
alsv_union(Set1, Set2, Res) :-
 	merge(Set1, Set2,DRes),
	list_to_set(DRes, Res),!.

alsv_intersect(Set1, Set2, Res) :-
	sub(Set1,Set2,R),
	sub(Set1,R,Res),!.    

alsv_except(Set1, Set2, Res) :-
	sub(Set1, Set2, Res),!.

alsv_completment(Att, Set1, Res) :-
	alsv_domain(Att, Domain, _),
	alsv_attr_class(Att, general),
	sub(Domain, Set1, Res),!.

nth1(1,[Value|_],Value).
nth1(Index,[OtherValue|Set],Value) :-
        nth1(NewIndex,Set,Value),
        Index is NewIndex+1.
		
list_to_set(List, Set) :-
     add_to_set(List,[],BackwardSet),
	 reverse(BackwardSet,Set).				%FIXME - not optimal
	 
add_to_set(X, S, S) :-
    atomic(X),
    member(X, S),
    !.
add_to_set(X, S, [X|S]) :-
    X \= [],
	atomic(X).	 
	 
add_to_set([], S, S).
add_to_set([H|T], S, S2) :-
    add_to_set(H, S, S1),
    add_to_set(T, S1, S2).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INFERENCE SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Single Table processing
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% table processing
runtables([]).
runtables([R|T]) :-
	runtable(R),
	trajectory_projection(R,table), 
	runtables(T).

runtable(T) :-
	xtable(T,_,X,Y),
	heart_debug(1,['Firing tabl: ','(',T,')',X,'==>',Y]),
	runrules(T),
	nl.

runrules(T):-
	xrule(T/R:C==>D),
	heart_debug(1,['Firing rule: ',T/R]),
	cond_satisfy(C),
	process_decision(D,Go),
	inference_transfer(Go),
	trajectory_projection(T/R,full), 
	fail.
runrules(_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GDI-Firing
%inference: GoalDriven GDI
%
%    * get set of goal tables
%    * build a sequential execution plan, by backward chaining using TT links, list of needed components
%    * run independent tables
%    * wave-like in steps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Goal driven for specified tables. Tables shoul be a list/set ex.: runrules_GDI([os]).
runrules_GDI(Goals) :-
	create_stack(OrderedTables,Goals),
	runtables(OrderedTables).
	
% Runs all tables in order they are linked in fact - this implements DDI inference
runrules_GDI :-
	get_goal_tables(Goals),
	create_stack(OrderedTables, Goals),
	runtables(OrderedTables).

create_stack([],[]).
create_stack(Stack,[HG|RG]) :-
		create_stack_helper(FrontStack, [HG]),
		create_stack(RareStack,RG),
		join_tables(FrontStack,RareStack,StackDuplicated),
		list_to_set(StackDuplicated,Stack),!.
	
create_stack_helper(Stack,Goal) :-
	get_pred_tables(Goal,Predecessors),
	join_tables(Predecessors,Goal,Stack).

get_pred_tables([],[]).
get_pred_tables(Goals,Predecessors) :-
	findall(C,condition_goal(C,Goals),Conds),
	flatten(Conds,FC),
	findall(T,decision_goal(T,FC),Preds),
	flatten(Preds,FP),
	get_pred_tables(FP,FPP),
	join_tables(FPP,FP,Predecessors), !.

join_tables([],X,X).
join_tables([X|L1],L2,[X|L3]) :-
	join_tables(L1,L2,L3).


condition_goal(C,Set) :-
	xtable(A,_,C,_),
	member(A,Set).
		
decision_goal(T,Set) :-
	xtable(T,_,_,D),
	nonvar(Set),
	intersection(D,Set,[_|_]). %If we assume that there may be links to more than one table.
	
decision_goal(T,Set) :-
	xtable(T,_,_,D),
	var(Set),
	Set=D.

find_goal_atts(GoalAtts) :-
	findall(D,xtable(_,_,_,D),Decs),
	findall(C,xtable(_,_,C,_),Conds),
	flatten(Decs,FD),
	flatten(Conds,FC),
	subtract(FD,FC,GoalAtts).
	
get_goal_tables(GTables) :-
	find_goal_atts(GA),
	findall(I,list_goaltabs(GA,I),GTables).
	
list_goaltabs(GA,I) :-	
	xtable(I,_,_,DN),
	subset(DN,GA).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Token-Driven-Firing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FIXME - it should follow the links instead of schema
% Create stack, but fill also create database containg data about inputs, (num of tokens tables require)
% Number of tokens are determined by number of R->T liks (not T->T as previously)

runrules_TDI(Goals):-
	retractall(xtoken(_,_,_)),
	create_token_stack(Stack,Goals),
	fill_token_knowledge(Stack),
	run_token_inference(Stack).
	
fill_token_knowledge([]).
fill_token_knowledge([Head|Tail]) :-
	get_reqtok_number(Head,Req),
	assert(xtoken(Head,0,Req)),
	fill_token_knowledge(Tail),!.
	
% +Table -Number
get_reqtok_number(Table,Number) :-
	get_connections(Table,ConnSet),
	get_direct(ConnSet,Direct),
	length(Direct,Number).
	
get_direct([],[]).
get_direct([[S,D]|Rest],DirectRest) :-
	findall(Route,get_routes(S,D,Route),RoutesDuplicated),
	list_to_set(RoutesDuplicated,Routes),
	length(Routes,L),
	L > 1,
	get_direct(Rest,DirectRest).

get_direct([[S,D]|Rest],[[S,D]|DirectRest]) :-
	findall(Route,get_routes(S,D,Route),RoutesDuplicated),
	list_to_set(RoutesDuplicated,Routes),
	length(Routes,L),
	L = 1,
	get_direct(Rest,DirectRest).
	
get_connections(Table, ConnSet) :-
	findall([S,Table],(xrule S/_:_==>_:Table/_ ; 
		xrule S/_:_==>_**>_:Table/_ ; 
		xrule S/_:_==>_:Table ; 
		xrule S/_:_==>_**>_:Table),
		Connections),
	list_to_set(Connections,ConnSet).
	
get_routes(S,D,[S,D]) :-
	(xrule S/_:_==>_:T/_ ;  
	xrule S/_:_==>_**>_:T/_ ; 
	xrule S/_:_==>_:T ; 
	xrule S/_:_==>_**>_:T),
	D=T.
	
get_routes(S,D,[S|Route]) :-
	(xrule S/_:_==>_:T/_ ; 
		xrule S/_:_==>_**>_:T/_ ; 
		xrule S/_:_==>_:T ; 
		xrule S/_:_==>_**>_:T),
		D\=T,
		get_routes(T,D,Route).

create_token_stack(Stack,Goals) :-
	token_stack_helper(StackPartial,Goals),
	flatten(StackPartial,Stack).

token_stack_helper([],[]).
token_stack_helper([SH|Stack],[GH|Goals]) :-
	findall(Route,get_routes(_,GH,Route),RoutesDuplicated),
	list_to_set(RoutesDuplicated,Routes),
	union_paths(Routes,SH),
	token_stack_helper(Stack,Goals).
	

union_paths([PH|[]],PH).
union_paths([PH|Paths],Union) :-
	union_paths(Paths,PartialUnion),
	union(PH,PartialUnion,Union).


run_token_inference([]).
run_token_inference([Head|Stack]) :-
	runtable_TDI(Head),
	trajectory_projection(Head,table), 
	run_token_inference(Stack).

runtable_TDI(T) :-
	% Check if tokens are OK
	xtoken(T,Req,Req),
	xtable(T,_,X,Y),
	heart_debug(1,['Firing tabl: ','(',T,')',X,'==>',Y]),
	runrules(T),
	nl,!.
runtable_TDI(_).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DDI-Firing
%inference: DataDriven
%
%  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Goal driven for specified tables. Tables shoul be a list/set ex.: runrules_DDI([ms,dt,th]).
runrules_DDI(Init) :-
	create_ddi_stack(OrderedTables,Init),
	runtables(OrderedTables).
	
% Runs all tables in order they are linked
runrules_DDI :-
	init_tables(Init),
	create_ddi_stack(OrderedTables, Init),
	runtables(OrderedTables).

create_ddi_stack([],[]).
create_ddi_stack(Stack,[H|Goals]) :-
	create_ddi_stack_helper(StackPart1, H),
	create_ddi_stack(StackPart2,Goals),
	join_tables(StackPart1,StackPart2,StackDuplicated),
	reverse(StackDuplicated,RStackDuplicated),
	list_to_set(RStackDuplicated,RStack),
	reverse(RStack,Stack),!.
	
	
create_ddi_stack_helper(Stack,Table) :-
	get_succ_tables([Table],Successors),
	join_tables([Table],Successors,Stack),!.

get_succ_tables([],[]).
get_succ_tables(Tables,Successors) :-
	findall(DecsAtts,(xtable(Table,_,_,DecsAtts),member(Table,Tables)),AllDecsAtts),
	flatten(AllDecsAtts,FlatDecsAtts),
	findall(SucTable,(xtable(SucTable,_,CondAtts, _), 
	   intersection(FlatDecsAtts,CondAtts,Result),
           Result \= []),
           SucTables),
	flatten(SucTables, SuccessorsPart1),		%??
	get_succ_tables(SuccessorsPart1,SuccessorsPart2),
	join_tables(SuccessorsPart1,SuccessorsPart2,Successors),!.
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RULES SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rule in table processing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rule processing
cond_satisfy([]).
cond_satisfy([C|Rest]) :-
	formula_satisfied(C),!,
	cond_satisfy(Rest).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% here comez the alsv magic
% fill all the clauses implementing different operators
% for both simple and general attributes
formula_satisfied(C) :-
	alsv_valid(C,current),!,
	heart_debug(1,['Condition: ',C,' satisfied']).

%DEBUG OBLY
formula_satisfied(C) :-
	heart_debug(1,['Condition: ',C,' NOT satisfied']),!,fail.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


inference_transfer(T/R) :-
	give_token(T),!,
	heart_debug(1,['Go to:',T/R]).
inference_transfer(T) :-
	give_token(T),!,
	heart_debug(1,['Go to:',T]).
inference_transfer(none) :-
	heart_debug(1,['Go to:','NONE']),		
	!.

give_token(Table) :-
	xtoken(Table,Get,Req),
	Get < Req,
	retractall(xtoken(Table,_,_)),
	IncGet is Get+1,
	assert(xtoken(Table,IncGet,Req)),!.
give_token(_).

process_decision(D**>A:G,G) :-
	process_dec_attributes(D),
	process_dec_actions(A),
	!.
process_decision(D**>A,none) :-
	process_dec_attributes(D),
	process_dec_actions(A),
	!.
process_decision(D:Go,Go) :-
	process_dec_attributes(D),
	!.
process_decision(D,none) :-
	process_dec_attributes(D),
	!.

process_dec_attributes([]).
process_dec_attributes([At|Rest]):-
	process_decision(At),
	process_dec_attributes(Rest).

process_dec_actions([]).
process_dec_actions([Ac|Rest]):-
	process_action(Ac),
	process_dec_actions(Rest).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% here comez the decision attribute manipulation magic
% fill all the clauses implementing different cases
% including set, setheoretic, arithm expressions
% for both simple and general attributes
% this needs to address some middle layer to handle blackboard
process_decision(Att) :-
	alsv_transition(Att).

process_decision(Att) :-
	heart_debug(1,['Decision act: ',Att,'NOT handled!']).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% here comez the decision action execution magic
% fill all the clauses implementing different cases
% executing preregistered predicates, prolog predicates, java?
% this needs to address some middle layer to handle blackboard
process_action([ActionName, Parameters]) :-
	xactn ActionName : Parameters >>> Goal,
	call(Goal),
	heart_debug(1,['Decision act: ',ActionName,'processes.']).	
	% check if it is a registered action
	% check if arguments are OK
	% call(Ac), % hopefully it would not expolode?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init+debug helper stuff
find_free_atts(FreeAtts) :-
	findall(D,xtable(_,_,_,D),Decs),
	findall(C,xtable(_,_,C,_),Conds),	
	flatten(Decs,FD),
	flatten(Conds,FC),
	subtract(FC,FD,FreeAtts).

init_tables(RTables) :-
	find_free_atts(FA),
	findall(I,list_initabs(FA,I),RTables).
list_initabs(FA,I) :-	
	xtable(I,_,CN,_),
	subset(CN,FA).

xtables_show :-
	xschm S: _ ==> _,
	xtable_show(S),
	fail.
xtables_show.

xtable_show(T) :-
	xrule T/R: _ ==> _,
	write(T/R), nl,
	fail.
xtable_show.

xtable(Name,Desc,From,To) :-
	xschm Name/Desc : From ==> To.
xtable(RealName,Desc,From,To) :-
	xschm RealName : From ==> To,
	RealName \= _/Desc.
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RULES SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
	
state_copy(From,To) :-
	From \= To,
	retractall(xstat To:_),
	state_copy_helper(From,To).
	
state_copy(State,State).

state_copy_helper(From,To):-
	xstat From: Val,
	assert(xstat To: Val),
	fail.
	
state_copy_helper(_,_).
	
state_add(Statename, Statevalues) :-
	retractall(xstat Statename:_),
    state_add_helper(Statename,Statevalues).
	
state_add_helper(_,[]).
state_add_helper(Statename,[Value|Rest]) :-
	assert(xstat Statename:Value),
	state_add_helper(Statename,Rest).
	
state_get(Statename, Values) :-
	findall(Value,(xstat(Statename:Value)),Values).
	
state_get_explicite(Statename, Values) :-
	findall([Att,NormValue],(xstat(Statename:[Att,Value]),normalize(Att,Value,NormValue)),Values).
	

state_print(State) :-
	xstat State:_,
	write('State \"'), write(State), write('\": '),nl,
	state_print_helper(State),
	!.

state_print(State) :-
	heart_debug(1,['No state ',State,' defined.']).
	
state_print_helper(State):-
	xstat State:Val,
	write(Val),write(','),
	fail.
state_print_helper(_):-nl.

trajectory_projection(ProjectionName, Mode) :-
	% assert current state to new generated state
	trajectory_mode(Mode),
	stat_genid(SId),
	state_copy(current,SId),
	traj_id(TId),
	((xtraj TId : Trajectory);Trajectory = []),
	append(Trajectory,[[ProjectionName,SId]],CurrentTraj),
	retractall(xtraj(TId:_)),
	assert(xtraj TId : CurrentTraj),
	!.
trajectory_projection(_,_).
	
trajectory_set_mode(Mode) :-
	retractall(trajectory_mode(_)),
	assert(trajectory_mode(Mode)).

stat_genid(ID) :-
	stat_id(ID),
	\+xstat(ID:_),!.

	
stat_genid(IDCandidate) :-
	repeat,
	gensym(s,IDCandidate),
	retractall(stat_id(_)),
	assert(stat_id(IDCandidate)),
	\+xstat(IDCandidate: _),!.

traj_genid(ID) :-
	traj_id(ID),
	\+xtraj(ID : _),!.
	
traj_genid(_) :-
	repeat,
	gensym(t,IDCandidate),
	retractall(traj_id(_)),
	assert(traj_id(IDCandidate)),
	\+xtraj(IDCandidate: _),!.
	
gensym(SymbolicPrefix, Id) :-
    current_prolog_flag(max_integer,Seed),
	rand_int(Seed, NumberId),
	num_atom(NumberId,SymNumId),
	atom_concat(SymbolicPrefix,SymNumId,Id).

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IO SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CALLBACKS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%all atts having 'comm: in'
findall_input_atts(IA) :-
	findall(N,(xattr(X),member(name:N,X),member(comm:in,X)),IA).

%fire all callbacks in given atts. Search for callback in att definition call it using Prolog Call
fireall_callbacks([Att|Rest]) :-
	fire_callback(Att),
	fireall_callbacks(Rest).

fireall_callbacks([]).

fire_callback(Att) :-
	xattr(A),
	member(name:Att,A),
	member(callback:[Name,Params],A),
	xcall Name : Params >>> Goal,
	call(Goal).

%all atts having 'comm: out'
findall_output_atts(OA) :-
	findall(N,(xattr(X),member(name:N,X),member(comm:out,X)),OA).
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% UTILS SECTION COPIED FROM SWI-PROLOG %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

%%	intersection(+Set1, +Set2, -Set3) is det.
%
%	True if Set3 unifies with the intersection of Set1 and Set2.
%	The complexity of this predicate is |Set1|*|Set2|
%
%	@see ord_intersection/3.

intersection([], _, []) :- !.
intersection([X|T], L, Intersect) :-
	memberchk(X, L), !,
	Intersect = [X|R],
	intersection(T, L, R).
intersection([_|T], L, R) :-
	intersection(T, L, R).


%%	union(+Set1, +Set2, -Set3) is det.
%
%	True if Set3 unifies with the union of Set1 and Set2.
%	The complexity of this predicate is |Set1|*|Set2|
%
%	@see ord_union/3.

union([], L, L) :- !.
union([H|T], L, R) :-
	memberchk(H, L), !,
	union(T, L, R).
union([H|T], L, [H|R]) :-
	union(T, L, R).


%%	subset(+SubSet, +Set) is semidet.
%
%	True if all elements of SubSet belong to Set as well. Membership
%	test is based on memberchk/2.  The complexity is |SubSet|*|Set|.
%
%	@see ord_subset/2.

subset([], _) :- !.
subset([E|R], Set) :-
	memberchk(E, Set),
	subset(R, Set).


%%	subtract(+Set, +Delete, -Result) is det.
%
%	Delete all elements in Delete  from   Set.  Deletion is based on
%	unification using memberchk/2. The complexity is |Delete|*|Set|.
%
%	@see ord_subtract/3.

subtract([], _, []) :- !.
subtract([E|T], D, R) :-
	memberchk(E, D), !,
	subtract(T, D, R).
subtract([H|T], D, [H|R]) :-
	subtract(T, D, R).

	
merge([],L,L).  
merge([H|T],L,[H|M]) :- merge(T,L,M).


memberchk(Elem, List) :-
     once(member(Elem, List)).
