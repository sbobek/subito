import alice.tuprolog.*;

public class tuHeart{
// params: 0 - model, 1- list of tables to fire
  public static void main(String[] srg){
    	Prolog engine = new Prolog();
	try{
    	     Theory t = new Theory(new java.io.FileInputStream("./tuHeaRT/tuheart.pl"));
	     engine.setTheory(t);
	     
	     Theory thermostat = new Theory(new java.io.FileInputStream(srg[0]));
	     engine.addTheory(thermostat);

	     System.out.println("Theory Loaded...");

	     SolveInfo answer  = engine.solve("gox(input/1,"+srg[1]+",foi).");
	     answer = engine.solve("xstat current: State.");
	     System.out.println(answer.getSolution());
	    
	     while(engine.hasOpenAlternatives()){
		answer = engine.solveNext();
	     	System.out.println(answer.getSolution());
	     }

	     
	    }catch(Exception e){e.printStackTrace();}
  
	}

}
