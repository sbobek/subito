#!/bin/bash

# -----------------------------------------------------------------------
# configuration
root=`pwd`
droolspath='DroolsExec.jar'
jesspath='java -cp .:./Jess71p2/lib/jess.jar:./Jess71p2/lib/jsr94.jar jess.Main -f2'
tuhpath='java -cp .:./tuHeaRT/lib/2p.jar:./tuHeaRT tuHeart'

# %C - Name and command line arguments of the command being timed.
# %e - Elapsed real (wall clock) time used by the process, in seconds.
# %M - Maximum resident set size of the process during its lifetime, in Kilobytes.
# %K - Average total (data+stack+text) memory use of the process, in Kilobytes.
timecmd="/usr/bin/time"
timeargs='-f "%C; %e; %M; %K"'
TMO='-s 9 1m'

# -----------------------------------------------------------------------

# hello message
function hello {
    echo "This is SamUrai Batch Integration TOol (SUBITO) 2014"
    echo $@
}
# -----------------------------------------------------------------------

# how to use the tool
function usage {
    echo "Help text"
}
# -----------------------------------------------------------------------

# message when the script arguments are incorrect
function paramserror {
    echo "Params error"
}
# -----------------------------------------------------------------------

function header {
    echo "--------------------------------------------------------"
    echo "SUBITO: "${1}
    echo "--------------------------------------------------------"
}
# -----------------------------------------------------------------------

function footer {
    echo "--------------------------------------------------------"
    echo "SUBITO: "${1}
    echo "--------------------------------------------------------"
}
# -----------------------------------------------------------------------

function parseparams {

    # if no file is specified
    if [ -z ${1} ]  
    then
	paramserror
	usage
    fi

    basefilename=${1}
    droolsfile="${basefilename}.drl"
    xttfile="${basefilename}.pl"
    jessfile="${basefilename}.clp"
    ctfile="${basefilename}"
    jrefile="${basefilename}-jre"
    easyrulesfile="${basefilename}-EasyRules"

    if [ ! -e ${droolsfile} ]
    then
	    echo "The" ${droolsfile} "does not exists." 1>&2
	    paramserror
	    usage
    fi
    if [ ! -e ${xttfile} ]
    then
	    echo "The" ${xttfile} "does not exists." 1>&2
	    paramserror
	    usage
    fi
    if [ ! -e ${jessfile} ]
    then
	    echo "The" ${jessfile} "does not exists." 1>&2
	    paramserror
	    usage
    fi
    if [ ! -e "${ctfile}/Runner.class" ]
    then
            echo "The" ${ctfile} "does not exists." 1>&2
            paramserror
            usage
    fi
    if [ ! -e "${jrefile}/RunnerJRE.class" ]
    then
            echo "The" ${jrefile}'/RunnerJRE.class' "does not exists." 1>&2
            paramserror
            usage
    fi
    if [ ! -e "${easyrulesfile}/starts/Start.class" ]
    then
            echo "The" ${easyrulesfile}'/starts/Start.class' "does not exists." 1>&2
            paramserror
            usage
    fi

    echo ${droolsfile}"|"${xttfile}"|"${jessfile}"|"${ctfile}"|"${jrefile}"|"${easyrulesfile}
    
}
# -----------------------------------------------------------------------

function droolsexecution {
    filename=${1}
    header "Execution of ${filename} using Drools..."
    timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout java -jar ${droolspath} ${filename} 2>/dev/null| nl -n ln | sed 's,\t,DROOLS:  ,g'
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   DROOLS:  0; 0; 0\n"
    fi
    footer "Execution of ${filename} using Drools...done"
}
# -----------------------------------------------------------------------

function jessexecution {
    filename=${1}
    header "Execution of ${filename} using Jess..."
    timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout ${jesspath} ${filename}  2>/dev/null| nl -n ln | sed 's,\t,JESS:  ,g'
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   JESS:  0; 0; 0"
    fi
    footer "Execution of ${filename} using Jess...done"
}

function ctexecution {
    filename=${1}
    header "Execution of ${filename} using Context-toolkit..."
    cd ${filename} ; timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout java -Djava.net.preferIPv4Stack=true -cp ../../context-toolkit/lib/*:. Runner  2>/dev/null| nl -n ln | sed 's,\t,CT:  ,g' 
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   CT:  0; 0; 0"
    fi
    cd "$root"
    footer "Execution of ${filename} using context-toolkit...done"
}

function jreexecution {
    filename=${1}
    d=`pwd` 
    echo "File: $d"
    header "Execution of ${filename} using Java Rule Engine..."
    cd ${filename} ; timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout java -cp ../../java-rule-engine/lib/*:. RunnerJRE  2>/dev/null| nl -n ln | sed 's,\t,JRE:  ,g' 
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   JRE:  0; 0; 0"
    fi
    cd "$root"
    footer "Execution of ${filename} using Java Rule Engine...done"
}

function erexecution {
    filename=${1}
    d=`pwd`
    echo "File: $d"
    header "Execution of ${filename} using Easy Rules..."
    cd ${filename} ; timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout java -cp ../../easy-rules/lib/*:. starts/Start  2>/dev/null| nl -n ln | sed 's,\t,ER:  ,g' 
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   ER:  0; 0; 0"
    fi
    cd "$root"
    footer "Execution of ${filename} using Easy Rules...done"
}


# -----------------------------------------------------------------------

function xttexecution {
    filename=${1}
    header "Execution of ${filename} using HeaRT..."

    model=$(cat ${filename} | sed '/^%/d'| sed '/ensure/d' | tr '\n' ' ' )
    alltables=$(cat ${filename} | grep "xrule" | awk '{print $2}' | awk -F "/" '{print $1}' | uniq | sed s/\'//g | tr '\n' ' ')
    #alltables=$(cat ${filename} | grep xrule | awk -F / '{print $1}' | sed 's,xrule ,,g' | uniq | tr '\n' ' ')
    tables="-tabs "${alltables}
    inputstate=$(cat ${filename} | grep 'xstat input/1' | awk -F ":" '{print $2}' | sed 's,.*\[\(.*\)\].*,\1,g' | awk -F "," '{print "-A" $1 "=" $2}' | tr '\n' ' ')

    #exec 3<>/dev/tcp/127.0.0.1/8090
    timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout java -jar heartdroid.jar ${filename} ${tables} ${inputstate} 2>/dev/null | grep -v "certainty" | grep -v "Checking condition" | grep -v "HEART: Conflict set" | grep -v "HEART: Processing" | grep -v "succeeded." | nl -n ln | sed 's,\t,HRTD:  ,g'
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   HEART:  0; 0; 0"
    fi

    footer "Execution of ${filename} using HeaRT...done"
}
# -----------------------------------------------------------------------
function tuxttexecution {
    filename=${1}
    header "Execution of ${filename} using tuHeaRT..."

    model=$(cat ${filename} | sed '/^%/d'| sed '/ensure/d' | tr '\n' ' ' )
    alltables=$(cat ${filename} | grep "xrule" | awk '{print $2}' | awk -F "/" '{print $1}' | uniq | sed s/\'//g | tr '\n' ',')
    #alltables=$(cat ${filename} | grep xrule | awk -F / '{print $1}' | sed 's,xrule ,,g' | uniq | tr '\n' ' ')
    tables="["$(echo ${alltables}| rev | cut -c 2- | rev)"]"

    #exec 3<>/dev/tcp/127.0.0.1/8090
    timeout $TMO 'time' -f "%e; %M; %K" -o /dev/stdout ${tuhpath} ${filename} ${tables} 2>/dev/null | nl -n ln | sed 's,\t,TUHRTD:  ,g'
    if [ ${PIPESTATUS[0]} == "137" ] ; then
       echo "   TUHRTD:  0; 0; 0"
    fi

    footer "Execution of ${filename} using tuHeaRT...done"
}
# -----------------------------------------------------------------------

# SamUrai Batch TranslatiOn

# hello message
hello

# checking params and generating file names
files=$(parseparams $@)
droolsfile=$(echo $files | awk -F "|" '{print $1}')
xttfile=$(echo $files | awk -F "|" '{print $2}')
jessfile=$(echo $files | awk -F "|" '{print $3}')
ctfile=$(echo $files | awk -F "|" '{print $4}')
jrefile=$(echo $files | awk -F "|" '{print $5}')
erfile=$(echo $files | awk -F "|" '{print $6}')

echo  'rules;' $(less $xttfile | grep xrule | wc -l)' attributes;' $(less $xttfile | grep xattr | wc -l)' schemas;' $(less $xttfile | grep xschm | wc -l)
droolsexecution ${droolsfile}
xttexecution ${xttfile}
jessexecution ${jessfile}
tuxttexecution ${xttfile}
jreexecution ${jrefile}
ctexecution ${ctfile}
erexecution ${erfile}
# -----------------------------------------------------------------------
