%Header: RULES,ATTS,SCHEMAS,HEARTDROID_TIME,HEARTDROID_MEM ,DROOLS_TIME, DROOLS_MEM,JESS_TIME,JESS_MEM,TUHEART_TIME,TUHEART_MEM,CT_TIME,CT_MEM,JRE_TIME,JRE_MEM
CUTOFF = 60;
MEMS = [5 7 9 11 13 15 17] ;
TIMES = [4 6 8 10 12 14 16];

datasets = [];
datasets = cellstr(datasets);
data = [];
for i=1:length(datasets)
  data_new =  dlmread (datasets{i},',')(2:end,:);

  %cutoff 
  for idx = TIMES 
    eidx = (data_new(:,idx) == 0);
    data_new(eidx,idx) = CUTOFF+1;
    data_new(eidx,idx+1) = max(data_new(:,idx+1));
  end

  if(isempty(data))
     data  = data_new;
  else
     data = data .+ data_new;
  end
end

data = data ./ length(datasets);

for idx = TIMES 
    eidx = (data(:,idx) > CUTOFF);
    data(eidx,idx) = data(eidx,idx+1) = 0;
end

%%%% FIXME fixing -1 status
for idx = TIMES
   [max_val,max_idx] = max(data(:,idx));
   last = length(data(:,idx));
   for bad = last:-1:1
     if(data(bad,idx) < max_val/2)
        data(bad,idx) = data(bad,idx+1) = 0;
     else
       break;
     end
   end
end
%%%% FIXME fixing -1 status

[u_rules_col,idx] = unique(data(:,1),'first');
u_rules_data=data(idx,:);

[u_atts_col,idx] = unique(data(:,2),'first');
u_atts_data = data(idx,:);

[u_schms_col,idx] = unique(data(:,3),'first');
u_schms_data = data(idx,:);



colour_teal = [18 150 155] ./ 255;
colour_lightgreen = [94 250 81] ./ 255;
colour_green = [12 195 82] ./ 255;
colour_lightblue = [8 180 238] ./ 255;
colour_darkblue = [1 17 181] ./ 255;
colour_yellow = [251 250 48] ./ 255;
colour_peach = [251 111 66] ./ 255;
colour_pink = [240 50 205] ./ 255;
colour_orange = [255 69 0] ./ 255;
colour_brown = [139 69 19] ./ 255;
linewidth=2;



colors = [colour_pink;colour_teal;colour_orange;colour_brown;colour_green;colour_lightblue;colour_yellow;colour_peach];



%plot time againt no_rules
figure;
hold on;
coloridx = 1;
failers = [];
for idx = TIMES 
  correct_only = (u_rules_data(:,idx) != 0);

  plot(u_rules_data(correct_only,1),u_rules_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_rules_data(correct_only,1));
  if(sum(correct_only) != length(u_rules_data(:,idx)))
     failers = [failers ; u_rules_data(correct_only,1)(endingidx), u_rules_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end

title('Number of rules versus execution time');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)

plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');

%plot mem against no_rules
figure;
hold on;
coloridx = 1;
failers = [];
for idx = MEMS
  correct_only = (u_rules_data(:,idx) != 0);

  plot(u_rules_data(correct_only,1),u_rules_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_rules_data(correct_only,1));
  if(sum(correct_only) != length(u_rules_data(:,idx)))
     failers = [failers ; u_rules_data(correct_only,1)(endingidx), u_rules_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end
title('Number of rules versus memory used');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)
plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');

%plot itme against no_atts
figure;
hold on;
coloridx = 1;
failers = [];
for idx = TIMES  
  correct_only = (u_atts_data(:,idx) != 0);

  plot(u_atts_data(correct_only,2),u_atts_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_atts_data(correct_only,2));
  if(sum(correct_only) != length(u_atts_data(:,idx)))
     failers = [failers ; u_atts_data(correct_only,2)(endingidx), u_atts_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end

title('Number of attributes versus execution time');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)
plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');

%plot mem against no_atts
figure;
hold on;
coloridx = 1;
failers = [];
for idx = [5 7 9 11 13 15 17]
  correct_only = (u_atts_data(:,idx) != 0);

  plot(u_atts_data(correct_only,2),u_atts_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_atts_data(correct_only,2));
  if(sum(correct_only) != length(u_atts_data(:,idx)))
     failers = [failers ; u_atts_data(correct_only,2)(endingidx), u_atts_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end
title('Number of attributes versus memory used');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)
plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');


%plot itme against no_schms
figure;
hold on;
coloridx = 1;
failers = [];
for idx = TIMES  
  correct_only = (u_schms_data(:,idx) != 0);

  plot(u_schms_data(correct_only,3),u_schms_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_schms_data(correct_only,3));
  if(sum(correct_only) != length(u_schms_data(:,idx)))
     failers = [failers ; u_schms_data(correct_only,3)(endingidx), u_schms_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end

title('Number of schemas versus execution time');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)
plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');

%plot mem against no_schms
figure;
hold on;
coloridx = 1;
failers = [];
for idx = [5 7 9 11 13 15 17]
  correct_only = (u_schms_data(:,idx) != 0);

  plot(u_schms_data(correct_only,3),u_schms_data(correct_only,idx),'color',colors(coloridx,:),'linewidth',linewidth);
  [endingval,endingidx] = max(u_schms_data(correct_only,3));
  if(sum(correct_only) != length(u_schms_data(:,idx)))
     failers = [failers ; u_schms_data(correct_only,3)(endingidx), u_schms_data(correct_only,idx)(endingidx)];
  end
  coloridx = coloridx+1;
end
title('Number of schemas versus memory used');
[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
set(ho, 'linewidth',linewidth+2)
plot(failers(:,1), failers(:,2),'color','red','linewidth',linewidth, 'marker', 'x','linestyle','none');
axis('tight');

