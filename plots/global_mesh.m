%Header: RULES,ATTS,SCHEMAS,HEARTDROID_TIME,HEARTDROID_MEM ,DROOLS_TIME, DROOLS_MEM,JESS_TIME,JESS_MEM,TUHEART_TIME,TUHEART_MEM,CT_TIME,CT_MEM,JRE_TIME,JRE_MEM
CUTOFF = 60;
MEMS = [5 7 9 11 13 15 17] ;
TIMES = [4 6 8 10 12 14 16];

colour_teal = [18 150 155] ./ 255;
colour_lightgreen = [94 250 81] ./ 255;
colour_green = [12 195 82] ./ 255;
colour_lightblue = [8 180 238] ./ 255;
colour_darkblue = [1 17 181] ./ 255;
colour_yellow = [251 250 48] ./ 255;
colour_peach = [251 111 66] ./ 255;
colour_pink = [240 50 205] ./ 255;
colour_orange = [255 69 0] ./ 255;
colour_brown = [139 69 19] ./ 255;
linewidth=2;

colors = [colour_pink;colour_teal;colour_orange;colour_brown;colour_green;colour_lightblue;colour_yellow;colour_peach];

datasets1 = ['stats-psc_kalkulacja_upustow_cenowych-0.csv';'stats-psc_kalkulacja_upustow_cenowych-1.csv';'stats-psc_kalkulacja_upustow_cenowych-2.csv';'stats-psc_kalkulacja_upustow_cenowych-3.csv';'stats-psc_kalkulacja_upustow_cenowych-4.csv'];
datasets1 = cellstr(datasets1);
datasets2 = ['stats-psc_koszty_operacyjne_ocena_mozliwosci_finansowania-0.csv';'stats-psc_koszty_operacyjne_ocena_mozliwosci_finansowania-1.csv';'stats-psc_koszty_operacyjne_ocena_mozliwosci_finansowania-2.csv';'stats-psc_koszty_operacyjne_ocena_mozliwosci_finansowania-3.csv';'stats-psc_koszty_operacyjne_ocena_mozliwosci_finansowania-4.csv'];
datasets2 = cellstr(datasets2);
datasets3 = ['stats-psc_koszty_stale_ocena_mozliwosci_korekty_budzetu-0.csv';'stats-psc_koszty_stale_ocena_mozliwosci_korekty_budzetu-1.csv';'stats-psc_koszty_stale_ocena_mozliwosci_korekty_budzetu-2.csv';'stats-psc_koszty_stale_ocena_mozliwosci_korekty_budzetu-3.csv';'stats-psc_koszty_stale_ocena_mozliwosci_korekty_budzetu-4.csv'];
datasets3 = cellstr(datasets3);
datasets4 = ['stats-psc_koszty_stale_ocena_zasadnosci_wydatku-0.csv';'stats-psc_koszty_stale_ocena_zasadnosci_wydatku-1.csv';'stats-psc_koszty_stale_ocena_zasadnosci_wydatku-2.csv';'stats-psc_koszty_stale_ocena_zasadnosci_wydatku-3.csv';'stats-psc_koszty_stale_ocena_zasadnosci_wydatku-4.csv'];
datasets4 = cellstr(datasets4);
datasets5 = ['stats-psc_ocena_pracownika_ocena_pracownika-0.csv';'stats-psc_ocena_pracownika_ocena_pracownika-1.csv';'stats-psc_ocena_pracownika_ocena_pracownika-2.csv';'stats-psc_ocena_pracownika_ocena_pracownika-3.csv';'stats-psc_ocena_pracownika_ocena_pracownika-4.csv'];
datasets5 = cellstr(datasets5);
datasets6 = ['stats-psc_softhis_ksiegowosc_faktury-0.csv';'stats-psc_softhis_ksiegowosc_faktury-1.csv';'stats-psc_softhis_ksiegowosc_faktury-2.csv';'stats-psc_softhis_ksiegowosc_faktury-3.csv';'stats-psc_softhis_ksiegowosc_faktury-4.csv'];
datasets6 = cellstr(datasets6);
datasets7 = ['stats-psc_softhis_ksiegowosc_urlopy-0.csv';'stats-psc_softhis_ksiegowosc_urlopy-1.csv';'stats-psc_softhis_ksiegowosc_urlopy-2.csv';'stats-psc_softhis_ksiegowosc_urlopy-3.csv';'stats-psc_softhis_ksiegowosc_urlopy-4.csv'];
datasets7 = cellstr(datasets7);
datasets8 = ['stats-psc_softhis_projekty-0.csv';'stats-psc_softhis_projekty-1.csv';'stats-psc_softhis_projekty-2.csv';'stats-psc_softhis_projekty-3.csv';'stats-psc_softhis_projekty-4.csv'];
datasets8 = cellstr(datasets8);
datasets9 = ['stats-psc_zakup_ocena_dostawcy-0.csv';'stats-psc_zakup_ocena_dostawcy-1.csv';'stats-psc_zakup_ocena_dostawcy-2.csv';'stats-psc_zakup_ocena_dostawcy-3.csv';'stats-psc_zakup_ocena_dostawcy-4.csv'];
datasets9 = cellstr(datasets9);
datasets10 = ['stats-psc_zakup_weryfikacja_potrzeb-0.csv';'stats-psc_zakup_weryfikacja_potrzeb-1.csv';'stats-psc_zakup_weryfikacja_potrzeb-2.csv';'stats-psc_zakup_weryfikacja_potrzeb-3.csv';'stats-psc_zakup_weryfikacja_potrzeb-4.csv'];
datasets10 = cellstr(datasets10);
datasets11 = ['stats-psc_zakup_zatwierdzenie_zamowienia-0.csv';'stats-psc_zakup_zatwierdzenie_zamowienia-1.csv';'stats-psc_zakup_zatwierdzenie_zamowienia-2.csv';'stats-psc_zakup_zatwierdzenie_zamowienia-3.csv';'stats-psc_zakup_zatwierdzenie_zamowienia-4.csv'];
datasets11 = cellstr(datasets11);
datasets12 = ['stats-psc_zatrudnienie_ocena_kandydata-0.csv';'stats-psc_zatrudnienie_ocena_kandydata-1.csv';'stats-psc_zatrudnienie_ocena_kandydata-2.csv';'stats-psc_zatrudnienie_ocena_kandydata-3.csv';'stats-psc_zatrudnienie_ocena_kandydata-4.csv'];
datasets12 = cellstr(datasets12);



all_data = {datasets1,datasets2,datasets3,datasets4,datasets5,datasets6,datasets7,datasets8,datasets9,datasets10,datasets11,datasets12};

z = [];
xxx = [];
yyy= [];
zzz=[];
divide = [];
idx = 14; %HEARTDROID TIME
for i=1:length(all_data)
    [u_rules_data] = load_n_process(all_data{i},TIMES,CUTOFF);
    

    %build mesh matrix for specific engine
    correct_only = (u_rules_data(:,idx) != 0);

    x = max(u_rules_data(correct_only,1));
    y = max(u_rules_data(correct_only,3));
    
    new_x = max([x,size(z)(1)]);
    new_y = max([y,size(z)(2)]);
    if(size(z)(1) < new_x || size(z)(2) < new_y)
       z = resize(z,new_x,new_y);
       divide = resize(divide,new_x,new_y);
    end


    xxx = [xxx; u_rules_data(correct_only,1)];
    yyy = [yyy; u_rules_data(correct_only,3)];
    zzz = [zzz; u_rules_data(correct_only,idx)];
    

    for k=1:length(u_rules_data(correct_only,1))
       if(z(u_rules_data(correct_only,1)(k),u_rules_data(correct_only,3)(k)) != 0)
           divide(u_rules_data(correct_only,1)(k),u_rules_data(correct_only,3)(k))  += 1;
       end
       z(u_rules_data(correct_only,1)(k),u_rules_data(correct_only,3)(k)) += u_rules_data(correct_only,idx)(k);
    end
end

divide = divide .+ 1;
final_mesh=z ./ divide;

vals = xxx;
max_num = 0;
for i=1:length(vals)
  val = vals(i);
  s = sum(vals == val);
  if(s > max_num)
    max_num = s;
    best_rval = val;
  end
end

best_rval

ID = find(vals == best_rval);
X = yyy(ID);
Y = zzz(ID);

[Xs,IdxS] = sort(X);
Ys = Y(IdxS);

plot(Xs,Ys);

%title('Number of rules versus execution time');
%[h,ho]=legend('HeaRTdroid','Drools','Jess','tuHeaRT','Context Tookit','Jrule engine','Easy Rules');
%set(ho, 'linewidth',linewidth+2)

