function [data] = load_n_process(datasets,TIMES,CUTOFF)
    data = [];
    for i=1:length(datasets)
      data_new =  dlmread (datasets{i},',')(2:end,:);

      %cutoff 
      for idx = TIMES 
        eidx = (data_new(:,idx) == 0);
        data_new(eidx,idx) = CUTOFF+1;
        data_new(eidx,idx+1) = max(data_new(:,idx+1));
      end

      if(isempty(data))
         data  = data_new;
      else
         data = data .+ data_new;
      end
    end

    data = data ./ length(datasets);

    for idx = TIMES 
        eidx = (data(:,idx) > CUTOFF);
        data(eidx,idx) = data(eidx,idx+1) = 0;
    end

    %%%% FIXME fixing -1 status
    for idx = TIMES
       [max_val,max_idx] = max(data(:,idx));
       last = length(data(:,idx));
       for bad = last:-1:1
         if(data(bad,idx) < max_val/2)
            data(bad,idx) = data(bad,idx+1) = 0;
         else
           break;
         end
       end
    end
    %%%% FIXME fixing -1 status

    [u_rules_col,idx] = unique(data(:,1),'first');
    data=data(idx,:);
    
endfunction
