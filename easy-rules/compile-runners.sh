#!/bin/bash

for file in `ls -d ../models/*-EasyRules/` ; do
  javac -cp $file:./lib/* ${file}objects/*.java ${file}rules/*.java ${file}starts/*.java
  echo '$file*~' | xargs | rm
 
done
