#!/bin/bash

for file in `ls -d ../models/*-jre/` ; do
  javac -cp $file:./lib/* $file'RunnerJRE.java' ${file}engine/*.java
  echo '$file*~' | xargs | rm
 
done
